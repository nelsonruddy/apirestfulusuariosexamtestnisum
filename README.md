<h1 align="center">:computer: Usuario Service Springboot Api NISUM Test :computer:</h1> 

<p align="center">Desarrolle una aplicación que exponga una API RESTful de creación de usuarios. </p>

## Built With :technologist:

- Java 8.1
- JPA
- MapStruct
- Junit 5
- Memory database
- Swagger

## PostData:

-Swagger documentation access: http://localhost:8080/swagger-ui.html#/ 

 ## Links :link:
- [LinkedIn](https://www.linkedin.com/in/nelson-paulino/ "LinkedIn")

- [Twitter](https://twitter.com/ruddythedd "Twitter")

- [Instagram](https://www.instagram.com/nelson.ruddy/ "Instagram")

## Author

**Nelson Paulino**

##  Support 🤝

Contributions, issues and feature requests are welcome !





 
 
 
