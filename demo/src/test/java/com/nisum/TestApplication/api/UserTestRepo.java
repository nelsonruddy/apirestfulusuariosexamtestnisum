package com.nisum.TestApplication.api;

import com.nisum.TestApplication.api.Entities.User;
import com.nisum.TestApplication.api.Repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.List;
import java.util.Optional;


@DataJpaTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserTestRepo {

    @Autowired
    UserRepository userRepository;

    // JUnit test for saveEmployee
    @Test
    @Order(2)
    @Rollback(value = false)
    public void getListOfEmployeesTest(){

        List<User> users = userRepository.findAll();

        Assertions.assertThat(users.size()).isGreaterThan(0);

    }
    @Test
    @Order(3)
    @Rollback(value = false)
    public void getById(){

        User user = userRepository.findById(1L).get();

        if(user.getId() > 0){
            Assertions.assertThat(user.getId() > 0);
        }
    }


    @Test
    @Order(1)
    @Rollback(value = false)
    public void saveEmployeeTest(){


        User employee = User.builder()
               // .id(1L) descomentar para realizar update del id deseado.
                .name("Aliko")
                .email("ramesh@gmail.com")
                .password("villamella")
                .datecreated(null)
                .status("ENABLED")
                .build();

        userRepository.save(employee);

        Assertions.assertThat(employee.getId()).isGreaterThan(0);
    }

}
