package com.nisum.TestApplication.api.DTO;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.nisum.TestApplication.api.Entities.Information;
import com.nisum.TestApplication.api.Entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

@AllArgsConstructor
@Data
public class UsersDTO implements Serializable {


    private Long id;
    private String name;
    private String email;
    private String password;
    private List<Information> phones;
    private ZonedDateTime datecreated;
    private ZonedDateTime datemodified;
    private String status;

}
