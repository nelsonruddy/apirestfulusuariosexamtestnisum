package com.nisum.TestApplication.api.Entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "information")
public class Information {
  @JsonIgnore
  @Id()
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String number;
  private String citycode;
  private String contrycode;

  //relevant getters, setters, equals, etc
}
