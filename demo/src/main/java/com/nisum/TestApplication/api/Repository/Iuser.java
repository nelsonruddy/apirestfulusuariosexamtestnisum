package com.nisum.TestApplication.api.Repository;

import com.nisum.TestApplication.api.Entities.User;

import java.util.List;
import java.util.Optional;

public interface Iuser {

    List<User> getAllUsers();
    Optional<User> getUsersById(Long id);
    void saveUser(User user);
    void UpdateUser(User user);
    void delete(Long id);
    boolean findByEmail(String search);

}
