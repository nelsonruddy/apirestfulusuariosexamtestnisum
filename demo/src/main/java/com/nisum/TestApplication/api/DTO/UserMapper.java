package com.nisum.TestApplication.api.DTO;

import com.nisum.TestApplication.api.Entities.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    List<UsersDTO> toUsersDTOs(List<User> users);

    User toUser(UsersDTO usersDTO);

    UsersDTO toUsersDTO(User userDTO);


}
