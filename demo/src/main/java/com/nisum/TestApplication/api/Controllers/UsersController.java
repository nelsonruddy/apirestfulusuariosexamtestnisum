package com.nisum.TestApplication.api.Controllers;

import com.nisum.TestApplication.api.DTO.UsersDTO;
import com.nisum.TestApplication.api.DTO.UserMapper;
import com.nisum.TestApplication.api.Entities.User;
import com.nisum.TestApplication.api.Services.UserServices;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
public class UsersController {

    private final UserServices userServices;


    private final UserMapper userMapper;

    //Get all the customers
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<List<UsersDTO>> getAllCustomer() {
        List<User> users = userServices.getAllUsers();
        return ResponseEntity.ok(userMapper.toUsersDTOs(userServices.getAllUsers()));
    }

    //Get a Customer by id
    @GetMapping(value = "/{id}" , produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<UsersDTO> getCustomer(@PathVariable("id") long id) {
        Optional<User> customer = userServices.getUsersById(id);

        return ResponseEntity.ok(userMapper.toUsersDTO(customer.get()));
    }

    //Delete a customer
    @DeleteMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<User> deleteCustomer(@PathVariable("id") long id) {

        userServices.delete(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    //Save users.
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<UsersDTO> saveUser(@RequestBody UsersDTO usersDTO) {

        userServices.saveUser(userMapper.toUser(usersDTO));
        return ResponseEntity.status(HttpStatus.CREATED).body(usersDTO);
    }

    //Save or update a Customer.
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    private ResponseEntity<UsersDTO> updateUser(@RequestBody UsersDTO usersDTO) {
        userServices.UpdateUser(userMapper.toUser(usersDTO));

        return ResponseEntity.status(HttpStatus.CREATED).body(usersDTO);
    }



}