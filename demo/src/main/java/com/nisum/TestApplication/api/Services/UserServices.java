package com.nisum.TestApplication.api.Services;

import com.nisum.TestApplication.api.Entities.Information;
import com.nisum.TestApplication.api.Entities.User;
import com.nisum.TestApplication.api.Repository.InformationRepository;
import com.nisum.TestApplication.api.Repository.UserRepository;
import com.nisum.TestApplication.api.Repository.Iuser;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServices implements Iuser {

    private final UserRepository userRepository;
    private final InformationRepository informationRepository;

    //getting all records
    @Override
    public List<User> getAllUsers(){

        return userRepository.findAll();
    }


  //getting a specific record
    @Override
    public Optional<User> getUsersById(Long id){

        return userRepository.findById(id);
    }

   //saving or updating a record
   @Override
   @Transactional
    public void saveUser(User user)
    {
        user.setStatus("Active");
        userRepository.save(user);

    }

  //saving users
  @Override
  public void UpdateUser(User user)
  {

    userRepository.save(user);
  }



    //deleting a specific record
    @Override
    public void delete(Long id)
    {
        userRepository.deleteById(id);
    }

  //method to search a record by username or status
  @Override
  public boolean findByEmail(String search){


   return userRepository.findByEmail(search);

  }



}
